"""
This package provides higher-level functions to interact with the JSON API to FAIRDOM SEEK (0.3)
"""
from time import sleep
import requests
import os
import re


# import fleep
# import urllib
# from urllib.error import HTTPError
# import logging

# logging.basicConfig(level=logging.DEBUG)


# def return_list_of_matching_files(base_url, username, password, searchterm, keyword):
#    session = create_session(username, password)
#    search_results = search(searchterm, session, base_url)
#    return filter_search_results(search_results, keyword)


def create_session(auth_dict, verbose=False):
    """create requests session"""
    headers = {
        #"Content-type": "application/vnd.api+json",
        "Accept": "application/vnd.api+json",
        #"Accept-Charset": "ISO-8859-1"
    }
    session = requests.Session()
    username = auth_dict['username']
    if 'token' in auth_dict:
        if verbose:
            print('using token for authentication')
        token = auth_dict['token']
        headers["Authorization"] = token
    else:
        if verbose:
            print('using password for authentication')
        password = auth_dict['password']
        session.auth = (username, password)
    if verbose:
        print(f'created session for {username}')
    session.headers.update(headers)
    return session


def remove_leading_zeros(match):
    print('test3')
    has_seen_decimal = False
    new_name = ''
    for char in match:
        if char == '0' and not has_seen_decimal:
            # print('removing 0')
            continue
        if char.isdecimal():
            has_seen_decimal = True
        new_name = new_name + char
    if has_seen_decimal:
        return new_name
    else:
        return False


def add_leading_zeros(term):
    leading_digit = term[0].isdigit()
    """divide term into string and number"""
    number = ''
    string = ''
    for char in term:
        if char.isdecimal():
            number = number + char
        else:
            string = string + char
    # leading zero to number part for consistent 3 digits
    while len(number) < 3:
        number = '0' + number
        # print(f'adding leading 0 to searchterm: {number}')
    if leading_digit:
        new_term = number + string
    else:
        new_term = string + number
    return new_term


def keyword_in_title(search_result, keyword):
    """simple check if title contains keyword"""
    title = search_result['attributes']['title']
    return keyword in title


def keyword_in_title_regex(search_result, keyword, verbose=False):
    """simple check if title contains keyword"""
    title = search_result['attributes']['title']
    return match_regex(title, keyword, verbose)


def match_regex(title, keyword, verbose=False):
    if verbose:
        print(f'searching phrase \"{title}\" for keyword \"{keyword}\"')
    match = re.search(keyword, title)
    if match:
        return match.group(0)
    else:
        return False


def plural_of(object_type):
    if object_type in ['data_files', 'assays', 'studies', 'investigations', 'projects',
                          'programmes']:
        plural = object_type
    else:
        if object_type == 'study':
            plural = 'studies'
        else:
            plural = object_type + 's'
    return plural


def singular_of(object_type):
    if object_type in ['data_files', 'assays', 'studies', 'investigations', 'projects', 'programmes']:
        if object_type == 'studies':
            singular = 'study'
        else:
            singular = object_type[:-1]
    else:
        singular = object_type

    return singular




def get_title_of_object(server_dict, object_id, object_type, verbose=False):
    base_url = server_dict['base_url']
    plural = plural_of(object_type)
    link_to_object = base_url + f'/{plural}/{object_id}'
    session = create_session(server_dict['auth_dict'])
    response = session.get(link_to_object, verify=server_dict['working_certificate'])
    while response.status_code != 200:
        print(f'response: {response.json()}')
        print('sleeping and trying again')
        sleep(2)
        response = session.get(link_to_object, verify=server_dict['working_certificate'])
    if verbose:
        print(f"\tsuccessfully got title: {response.json()['data']['attributes']['title']}")
    return response.json()['data']['attributes']['title']



def check_if_file_exists(filename, server_dict):
    session = create_session(server_dict['auth_dict'])
    base_url = server_dict['base_url']
    list_of_all_files = session.get(base_url + '/data_files', verify=server_dict['working_certificate']).json()['data']
    for file in list_of_all_files:
        title = file['attributes']['title']
        if title == filename:
            return True
    return False

def check_if_file_exists_in_associated_assays(filename, assay_ids, server_dict):
    session = create_session(server_dict['auth_dict'])
    base_url = server_dict['base_url']
    for assay_id in assay_ids:
        link_to_asset = f'{base_url}/assays/{assay_id}'
        asset_response = session.get(link_to_asset, verify=server_dict['working_certificate'])

        for data_file_pair in asset_response.json()['data']['relationships']['data_files']['data']:
            id = data_file_pair['id']
            file_response= session.get(f'{base_url}/data_files/{id}', verify=server_dict['working_certificate']).json()['data']
            title = file_response['attributes']['title']
            if title == filename:
                return True
    return False



def write_results_to_file(successfully_uploaded_files, unsuccessfully_uploaded_files, log_folder='logging_output'):
    os.makedirs(log_folder, exist_ok=True)

    f = open(f"{log_folder}/successful.txt", "w")
    for entry in successfully_uploaded_files:
        f.write(f'{entry}\n')
    f.close()

    f = open(f"{log_folder}/unsuccessful.txt", "w")
    for entry in unsuccessfully_uploaded_files:
        f.write(f'{entry}\n')
    f.close()


def return_file_from_id(session, server_dict, file_id: str, verbose):


    success = False

    '''retrieve link to data file'''
    link_to_file = server_dict['base_url'] + f'/data_files/{file_id}'
    r = session.get(link_to_file, verify=server_dict['working_certificate'])

    if r.status_code != 200:
        if r.status_code == 404:
            print(
                f'The requested file {link_to_file} does not exist or has been deleted (status code: {r.status_code}).')
        elif r.status_code == 403:
            print(f'You are not authorized to access the file {link_to_file} (status code: {r.status_code}).')
        else:
            print(f'Failed to reach {link_to_file}, status code:{r.status_code}')
        return success, r.status_code, link_to_file
    else:
        #experimentally change header of ession
        session.headers.update({'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
                                'Accept-Encoding': 'gzip'#, deflate, br',
                                #'Content-type': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                                })
        success = True
        file_name = r.json()['data']['attributes']['title']
        #retrieve link to content blobs
        download_link = r.json()['data']['attributes']['content_blobs'][0]['link'] + '/download'
        if verbose:
            print(f"Title: {r.json()['data']['attributes']['title']}")
            print(f"Tags: {r.json()['data']['attributes']['tags']}")
            print(f"Download link: {download_link}")
        download_response = session.get(download_link, verify=server_dict['working_certificate'])

        return success, download_response.content, file_name




def uniquify_path(path):
    """ creates a unique path for storing datafiles when downloading
        e.g. if filename already exists, add number
    """
    filename, extension = os.path.splitext(path)
    counter = 1
    while os.path.exists(path):
        path = filename + " (" + str(counter) + ")" + extension
        counter += 1
    return path


def return_asset_metadata(server_dict, asset_type, asset_id):
    """return json of asset"""
    base_url = server_dict['base_url']
    session = create_session(server_dict['auth_dict'])
    plural = plural_of(asset_type)
    link_to_asset = f'{base_url}/{plural}/{asset_id}'
    response = session.get(link_to_asset, verify=server_dict['working_certificate'])
    if response.status_code == 200:
        asset_info = session.get(link_to_asset, verify=server_dict['working_certificate']).json()['data']
        return asset_info
    else:
        print(f'response to get request: {response.status_code}')
        return response

def verify_successful_login(server_dict):
    session = create_session(server_dict['auth_dict'])
    # TODO link to a (permanent) file that is visible to programm KFO5002 but NOT public
    link = 'https://c099-021.cloud.gwdg.de/data_files/2719'
    r = session.get(link, verify=server_dict['working_certificate'])
    if (r.status_code != 200) or ('errors' in r.json()):
        successful_login = False
        print(f"{color.RED}{color.BOLD}Login failed. Please try again.{color.END}")
        # print(f'Status code from login: {r.status_code}')
    else:
        successful_login = True
        print(f"{color.GREEN}{color.BOLD}Login successful.{color.END}")
    return successful_login


class color:
    PURPLE = '\033[95m'
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'

# code fragments:
# response.headers

def get_mime_type(fname):
    """has to written into ['attributes']['content_blobs'][0]['content_type']"""
    # Provide minimal detection of important file
    # types to make files compatible with seek
    if fname.endswith(".html"):
        return "text/html"#check
    if fname.endswith(".css"):
        return "text/css"#check
    if fname.endswith(".png") or fname.endswith(".jpg"):
        return "image"#check
    if fname.endswith(".xml"):
        return 'text/xml'#check
    if fname.endswith(".pdf"):
        return 'application/pdf'
    if fname.endswith(".xlsx"):
        return 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    if fname.endswith(".txt"):
        return 'text/plain'
    if fname.endswith(".csv"):
        return "text/csv"
    if fname.endswith(".bmp"):
        return "image/bmp"
    return "text/plain"


next_hierarchy = {'programmes':'projects', 'projects':'investigations', 'investigations':'studies', 'studies':'assays', 'assays':'data_files'}
