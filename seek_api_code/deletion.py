from seek_api_code import helper_functions as hf

def recursively_delete_asset(server_dict, asset_id, asset_type, delete_itself=False, verbose=False, exceptions=[],
                             dry_run=False):
    """use with caution!
        Parameters:
            asset_type(str): this can be one of the following:
                ['data_file', 'assay', 'study', 'investigation', 'project', 'programme',
                'data_files', 'assays', 'studies', 'investigations', 'projects', 'programmes']
                :param dry_run: only iterate over the structure and print what would be deleted without actually deleting
                :param exceptions: list of assets that should not be deleted, e.g.: ('investigations', '9'), ('studies', '18')
    """
    if (asset_type[-1] != 's'):
        asset_type = hf.plural_of(asset_type)
        if verbose:
            print(asset_type)
    link_to_asset = f'{server_dict["base_url"]}/{asset_type}/{asset_id}'
    session = hf.create_session(server_dict['auth_dict'])
    asset_response = session.get(link_to_asset, verify=server_dict['working_certificate'])

    deletion_recursion = ['data_files', 'assays', 'studies', 'investigations', 'projects',
                          'programmes']  # we need to erase in this order
    for type_to_delete in deletion_recursion:
        if type_to_delete == asset_type:
            if delete_itself:
                title = asset_response.json()['data']['attributes']['title']
                # if verbose:
                print(f'deleting {title} of type {type_to_delete}')
                deletion_response = session.delete(link_to_asset, verify=server_dict['working_certificate'])
                if (deletion_response.status_code != 200):
                    print(f'response of deletion: {deletion_response.json()}')
            break
        else:
            # if verbose:
            number_of_objects = len(asset_response.json()['data']['relationships'][type_to_delete]['data'])
            print(f"found {number_of_objects} {type_to_delete}")
            i = 0
            if (asset_response.status_code != 200):
                print(f'possible error: {asset_response}')
            for recursive_asset in asset_response.json()['data']['relationships'][type_to_delete]['data']:
                recursive_asset_id = recursive_asset['id']
                recursive_asset_type = recursive_asset['type']  # kind of redundant
                link_to_recursive_asset = f'{server_dict["base_url"]}/{recursive_asset_type}/{recursive_asset_id}'
                recursive_response = session.get(link_to_recursive_asset, verify=server_dict['working_certificate'])
                title = recursive_response.json()['data']['attributes']['title']
                if (recursive_asset_type, recursive_asset_id) not in exceptions:
                    if verbose:
                        print(f'deleting {title} of type {recursive_asset_type}, id: {recursive_asset_id}')
                    if not dry_run:
                        deletion_response = session.delete(link_to_recursive_asset, verify=server_dict['working_certificate'])
                        if (deletion_response.status_code != 200):
                            print(deletion_response.json())
                else:
                    print(f'skipping {title} of type {recursive_asset_type}, id: {recursive_asset_id}')
                i += 1
                print(f'{i}/{number_of_objects}')
