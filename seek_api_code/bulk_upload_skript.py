from seek_api_code import servers as s
from seek_api_code import create_assets as create
import os


# server_dict = s.our_testing_server_dict
# manager_ids =[62, 63]
# associated_programme_id = 3
# associated_project_id = 32

# folder_structure_dict = {}
# folder_structure_dict['investigations'] = {}
# folder_structure_dict['investigations']['title'] = "pdf_test_upload_investigation"
#
# folder_structure_dict['studies'] = {}
# folder_structure_dict['studies']['pdf_test_upload_study'] = {}
# folder_structure_dict['studies']['pdf_test_upload_study']['assays'] = {}
# folder_structure_dict['studies']['pdf_test_upload_study']['assays']['pdf_test_upload_assay'] = {}
#-----------------------------------------------------------------------------------------------------------------


#-------------------------------------------------------------------------------------------

server_dict = s.official_testing_server_dict#api_admin_server_dict
manager_ids = [server_dict['my_id']]#[12,15,7, 10, 11]
associated_programme_id = 37
associated_project_id = 162


folder_structure_dict = {}
folder_structure_dict['investigations'] = {}
folder_structure_dict['investigations']['title'] = 'pdf_test_upload_investigation'
#folder_structure_dict['investigations']['id'] = 368
#
# folder_structure_dict['studies'] = {}
# folder_structure_dict['studies']['CDX'] = {}
# folder_structure_dict['studies']['CDX']['id'] = 39
# folder_structure_dict['studies']['CDX']['assays'] = {}
# folder_structure_dict['studies']['CDX']['assays']['CDX Results'] = {}
# folder_structure_dict['studies']['CDX']['assays']['CDX Results']['id'] = 68
#
# folder_structure_dict['studies']['Organoids'] = {}
# folder_structure_dict['studies']['Organoids']['id'] = 3
# folder_structure_dict['studies']['Organoids']['assays'] = {}
# folder_structure_dict['studies']['Organoids']['assays']['Organoid Results'] = {}
# folder_structure_dict['studies']['Organoids']['assays']['Organoid Results']['id'] = 4
#
# folder_structure_dict['studies']['PDX'] = {}
# folder_structure_dict['studies']['PDX']['id'] = 4
# folder_structure_dict['studies']['PDX']['assays'] = {}
# folder_structure_dict['studies']['PDX']['assays']['PDX Results'] = {}
# folder_structure_dict['studies']['PDX']['assays']['PDX Results']['id'] = 6
#-----------------------------------------------------------------------------------------------------------------

folder_structure_dict['studies'] = {}
folder_structure_dict['studies']['pdf_test_upload_study'] = {}
#folder_structure_dict['studies']['pdf_test_upload_study']['assays'] = {}
#folder_structure_dict['studies']['pdf_test_upload_study']['assays']['Sample Submission Sheets'] = {}


investigation_folder_name = 'pdf_test_upload_investigation'
folder_path_absolute = os.path.join(os.path.dirname(os.path.dirname((os.path.dirname(os.getcwd())))), 'testdata', investigation_folder_name)

create.upload_from_folder_structure_to_investigation_flexible(folder_path_absolute, server_dict, associated_programme_id, folder_structure_dict,
                                                              associated_project_id, manager_ids, False, verbose=True)
