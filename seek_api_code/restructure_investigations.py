# restructure kfo programme according to karlys plan
# try that on schulungsserver first
# copy data structure from productive to schulungssever
# download data structure from productive server and store as json
# is there something similar already, that we can use?
# maybe recursively_delete_asset?
from typing import Dict, List, Callable

import logging

# Configure the logger
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

# Create a logger instance
logger = logging.getLogger(__name__)

import os

from seek_api_code import download as dl
from seek_api_code import servers as s
from seek_api_code import deletion as d
from seek_api_code import helper_functions as hf
from seek_api_code.helper_functions import get_title_of_object


import json
import requests  # Ensure you have the requests library installed
from collections import defaultdict

from pathvalidate import sanitize_filename

hierarchy_below = {
    'programmes': 'projects',
    'projects': 'investigations',
    'investigations': 'studies',
    'studies': 'assays',
    'assays': 'data_files'
}
hierarchy_above = {
    'projects': 'programmes',
    'investigations': 'projects',
    'studies': 'investigation',
    'assays': 'study',
    'data_files': 'assays',
}


def traverse_assets(server_dict, asset_id, asset_type, action, exceptions=None, verbose=False, dry_run=False):
    """
    Generic function to traverse and act on assets.

    Parameters:
        server_dict (dict): Configuration dictionary containing server details.
        asset_id (str): ID of the current asset.
        asset_type (str): Type of the current asset.
        action (callable): Function to execute on each asset. Should accept (session, asset_type, asset_id, asset_data, dry_run).
        exceptions (list of tuples): List of assets to skip, specified as (type, id).
        verbose (bool): If True, enables detailed logging.
        dry_run (bool): If True, simulates actions without performing them.
    """
    if exceptions is None:
        exceptions = []

    # Ensure asset_type is plural for constructing the URL
    asset_type_plural = hf.plural_of(asset_type) if not asset_type.endswith('s') else asset_type

    link_to_asset = f'{server_dict["base_url"]}/{asset_type_plural}/{asset_id}'
    session = hf.create_session(server_dict['auth_dict'])

    try:
        response = session.get(link_to_asset, verify=server_dict['working_certificate'])
        response.raise_for_status()
    except requests.RequestException as e:
        if verbose:
            print(f"Error fetching {asset_type} {asset_id}: {e}")
        return

    asset_data = response.json().get('data', {})

    # Execute the action on the current asset
    action(session, asset_type, asset_id, asset_data, dry_run)

    # Define the order of child asset types for traversal
    child_types = ['data_files', 'assays', 'studies', 'investigations', 'projects', 'programmes']

    for child_type in child_types:
        relationships = asset_data.get('relationships', {}).get(child_type, {}).get('data', [])
        if relationships:
            if verbose:
                print(f"Processing {len(relationships)} {child_type} under {asset_type} {asset_id}")
            for child in relationships:
                child_id = child.get('id')

                if (child.get('type'), child_id) in exceptions:
                    if verbose:
                        print(f"Skipping {child.get('type')} {child_id}")
                    continue

                # Recursive traversal
                traverse_assets(
                    server_dict,
                    child_id,
                    child.get('type', child_type),
                    action,
                    exceptions,
                    verbose,
                    dry_run
                )


def clean_foldername(name):
    cleaned = name.strip().replace(" ", "_")
    # Sanitize für sichere Verwendung als Dateiname
    return sanitize_filename(cleaned)

def process_asset_hierarchy(server_dict: Dict, asset_id: str, asset_type: str, action: Callable, log: Dict, parent_asset, *args,
                            exceptions: List[str] = None, verbose: bool = False) -> Dict:
    """
    Recursively retrieves and processes the hierarchical structure of assets from a server,
    potentially downloading specific files at the lowest level of the hierarchy.

    :param parent_asset:
    :param server_dict: A dictionary containing server connection details and authentication information.
    :param asset_id: The unique identifier of the current asset being processed.
    :param asset_type: The type of the current asset (e.g., 'programmes', 'projects', 'investigations', etc.).
    :param action (callable): Function to execute on each asset. Should accept (session, asset_type, asset_id, asset_data,).
    :param folder: The local folder path where downloaded files or created subfolders will be stored.
    :param exceptions: A list of asset titles to be excluded from processing.
    :param *args: Arguments that will be passed to the action function
    :param verbose: If True, enables detailed logging.
    """

    if exceptions is None: #failsave
        exceptions = []



    # Ensure asset_type is plural for constructing the URL
    asset_type = hf.plural_of(asset_type) if not asset_type.endswith('s') else asset_type

    # Fetch the title of the current asset
    title = get_title_of_object(server_dict, asset_id, asset_type, verbose=verbose)
    logger.info(f"Processing {hf.singular_of(asset_type)}: {title}")

    if title in exceptions:
        logger.info(f"Skipping asset '{title}' as it is in the exceptions list.")
        return

    # Construct the API link to the current asset
    link_to_asset = f"{server_dict['base_url']}/{hf.plural_of(asset_type)}/{asset_id}"

    # Create a session for API requests
    session = hf.create_session(server_dict['auth_dict'])

    try:
        response = session.get(link_to_asset, verify=server_dict['working_certificate'])
        response.raise_for_status()
        asset_data = response.json().get('data', {})
    except requests.RequestException as e:
        logger.error(f"Error fetching {asset_type} {asset_id}: {e}")
        return
    log_asset_action = log_asset_action_factory(log)
    log_asset_action(asset_data, parent_asset)
    # Execute the action on the current asset
    save_path = reconstruct_path_from_log(asset_type, asset_id, log)
    action(session, asset_data, exceptions, save_path, *args) #is it sensible to pass asset_data?
    # Access relationships
    relationships = asset_data.get('relationships', {})

    # Determine the next level in the hierarchy
    next_asset_type = hierarchy_below.get(asset_type)
    if not next_asset_type:
        logger.info(f"No further hierarchy defined for asset type '{asset_type}'.")
        return

    # Access the specific relationship data for the next level
    related_assets = relationships.get(next_asset_type, {}).get('data', [])

    if not related_assets:
        logger.info(f"No related assets found under '{next_asset_type}' for {asset_type} {asset_id}.")
        return

    # Iterate over each related asset
    for asset in related_assets:
        child_id = asset.get('id')
        child_type = asset.get('type')

        if not child_id or not child_type:
            logger.warning(f"Encountered related asset with missing 'id' or 'type': {asset}")
            continue

        # Fetch the title of the child asset
        child_title = get_title_of_object(server_dict, child_id, child_type, verbose=verbose)
        logger.info(f"Found related {hf.singular_of(child_type)}: {child_title}")

        if child_title in exceptions:
            logger.info(f"Skipping asset '{child_title}' as it is in the exceptions list.")
            continue

        # Recursive call to process the child asset
        process_asset_hierarchy(server_dict, child_id, child_type, action, log, asset_data, *args, exceptions=exceptions, verbose=verbose)
    return shared_log_dict





def delete_asset_action(session, asset_type, asset_id, asset_data, dry_run):
    """
    Action function to delete an asset.

    Parameters:
        session (requests.Session): The HTTP session for making requests.
        asset_type (str): Type of the asset.
        asset_id (str): ID of the asset.
        asset_data (dict): Data of the asset.
        dry_run (bool): If True, simulates deletion without performing it.
    """
    title = asset_data.get('attributes', {}).get('title', 'Unknown Title')
    print(f"Deleting {title} of type {asset_type}, ID: {asset_id}")

    if not dry_run:
        link_to_asset = f"{server_dict['base_url']}/{hf.plural_of(asset_type)}/{asset_id}"
        try:
            response = session.delete(link_to_asset, verify=server_dict['working_certificate'])
            response.raise_for_status()
            print(f"Successfully deleted {asset_type} {asset_id}")
        except requests.RequestException as e:
            print(f"Failed to delete {asset_type} {asset_id}: {e}")


def log_asset_action_factory(log_dict):
    """
    Factory to create a logging action function.

    Parameters:
        log_dict (defaultdict): Dictionary to store the logged structure.

    Returns:
        callable: A function that logs asset details into log_dict.
    """
    def log_asset_action(asset_data, parent_asset):
        asset_id = asset_data['id']
        asset_type = asset_data['type']

        #log_entry = create_log_entry_light(asset_data, asset_id, asset_type)
        log_entry = asset_data
        log_entry['parent_asset'] = {}
        if 'id' in parent_asset:
            log_entry['parent_asset']['id'] = parent_asset['id']
            log_entry['parent_asset']['type'] = parent_asset['type']
            log_entry['parent_asset']['title'] = parent_asset['attributes']['title']
            log_entry['path'] = os.path.join(parent_asset['path'], clean_foldername(asset_data['attributes']['title']))
        else:
            log_entry['path'] = clean_foldername(asset_data['attributes']['title'])


        log_dict[asset_type][asset_id] = log_entry



    return log_asset_action


def create_log_entry_light(asset_data, asset_id, asset_type):
    log_entry = {
        'id': asset_id,
        'type': asset_type,
        'title': asset_data.get('attributes', {}).get('title', ''),
        'relationships': {}
    }
    try:
        for rel_type, rel_info in asset_data.get('relationships', {}).items():
            if (rel_type == 'creators' or rel_type == 'submitter' or rel_type == 'people' \
                    or rel_type == 'models' or rel_type == 'sops' or rel_type == 'publications'
                    or rel_type == 'documents' or rel_type == 'organisms' or rel_type == 'human_diseases'
                    or rel_type == 'samples' or rel_type == 'placeholders' or rel_type == 'placeholder'
                    or rel_type == 'workflows' or rel_type == 'events' or rel_type == 'file_template'):
                continue
            data = rel_info.get('data', {})
            if isinstance(data, dict) and 'id' in data:
                # Single related asset
                related_ids = [data]
            elif isinstance(data, list):
                logger.warning(f"related asset data is list:{data}")
                # if asset_type == 'data_files':
                related_ids = data
                # else:
                #    related_ids = []
            else:
                # No related asset or invalid structure
                related_ids = []
            log_entry['relationships'][rel_type] = related_ids
    except Exception as e:
        logger.error(f"Error processing relationships: {str(e)}")
    #            log_entry['relationships'][rel_type] = []
    return log_entry



def reconstruct_path_from_log(asset_type, asset_id, log):
    path = log[asset_type][asset_id]['path']
    return path

def reconstruct_path_from_log_old(asset_type, asset_id, log):
    name_of_asset = log[asset_type][asset_id]['attributes']['title']
    path = name_of_asset
    while asset_id:
        path, asset_type, asset_id = append_parent_directory(path, log, asset_type, asset_id)
    return path

def append_parent_directory(path, log, asset_type, asset_id):
    parent_asset_info = log[asset_type][asset_id]['parent_asset']
    if parent_asset_info == {}:
        parent_asset_type =  parent_asset_id = False
        parent_path = path
    else:
        parent_folder = parent_asset_info['title']
        parent_path = os.path.join(clean_foldername(parent_folder), path)
        parent_asset_type = parent_asset_info['type']
        parent_asset_id = parent_asset_info['id']
    return parent_path, parent_asset_type, parent_asset_id

def recursively_delete_asset(server_dict, asset_id, asset_type, delete_itself=False, verbose=False, exceptions=None, dry_run=False):
    """
    Deletes assets recursively starting from the given asset.

    Parameters:
        server_dict (dict): Configuration dictionary containing server details.
        asset_id (str): ID of the root asset.
        asset_type (str): Type of the root asset.
        delete_itself (bool): If True, deletes the root asset as well.
        verbose (bool): If True, enables detailed logging.
        exceptions (list of tuples): List of assets to skip, specified as (type, id).
        dry_run (bool): If True, simulates deletion without performing it.
    """
    if exceptions is None:
        exceptions = []

    def conditional_delete_action(session, asset_type_inner, asset_id_inner, asset_data_inner, dry_run_inner):
        if delete_itself or asset_type_inner != asset_type:
            delete_asset_action(session, asset_type_inner, asset_id_inner, asset_data_inner, dry_run_inner)

    traverse_assets(
        server_dict,
        asset_id,
        asset_type,
        conditional_delete_action,
        exceptions,
        verbose,
        dry_run
    )



def download_asset(session, asset_data, exceptions, save_path): # pass session instead of dict?
    asset_id = asset_data['id']
    type_of_asset = asset_data['type']
    title = hf.get_title_of_object(server_dict, asset_id, type_of_asset, verbose=False)
    folder = asset_data['relationships'][hierarchy_above[type_of_asset]]
    if title not in exceptions:
        if type_of_asset != 'data_files' and len(save_path) > 0:
            # create folders with name of children assets
            os.makedirs(save_path, exist_ok=True)
        else:
            #download the file
            success, file_contents, file_name = hf.return_file_from_id(session, server_dict, asset_id, verbose=False)
            if success:
                dl.save_to_disc(file_contents, save_path)
                #successfully_downloaded_files.append(file_name)
            else:
                print(f'could not download {file_name}')

def log_investigation_structure(server_dict, investigation_id, output_file, verbose=False):
    """
    Logs the structure of an investigation to a JSON file.

    Parameters:
        server_dict (dict): Configuration dictionary containing server details.
        investigation_id (str): ID of the investigation to log.
        output_file (str): Path to the output JSON file.
        verbose (bool): If True, enables detailed logging.
    """
    log_dict = defaultdict(dict)
    traverse_assets(
        server_dict,
        investigation_id,
        'investigation',
        log_asset_action_factory(log_dict),
        exceptions=None,
        verbose=verbose,
        dry_run=True  # Ensures that no actions are performed
    )

    try:
        with open(output_file, 'w') as f:
            json.dump(log_dict, f, indent=4)
        print(f"Investigation structure logged to {output_file}")
    except IOError as e:
        print(f"Failed to write to {output_file}: {e}")


# Usage:


# To delete assets recursively:
# recursively_delete_asset(
#     server_dict=server_dict,
#     asset_id='investigation_id_here',
#     asset_type='investigation',
#     delete_itself=True,
#     verbose=True,
#     exceptions=[('study', '18'), ('investigation', '9')],
#     dry_run=False
# )

# To log the structure of an investigation:
#log_investigation_structure(
#    server_dict=server_dict,
#    investigation_id='368',
#    output_file='investigation_structure.json',
#    verbose=True
#)

server_dict = s.official_testing_server_dict #s.our_testing_server_dict #


log_dict = defaultdict(dict)
shared_log_dict = defaultdict(lambda: defaultdict(dict))


shared_log_dict = process_asset_hierarchy(server_dict=server_dict, asset_id=server_dict['existing_investigation_id'], asset_type='investigation', action=download_asset,
                                          log=shared_log_dict, parent_asset={})
print(shared_log_dict)
output_file = "log_output.json"
try:
    with open(output_file, 'w') as f:
        json.dump(shared_log_dict, f, indent=4)
    print(f"Investigation structure logged to {output_file}")
except IOError as e:
    print(f"Failed to write to {output_file}: {e}")