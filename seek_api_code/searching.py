#import seek_api.helper_functions as hf
from seek_api_code import helper_functions as hf


def return_ids_and_titles_from_seek_search(searchterm, return_unique, server_dict, verbose):
    """returns list of file IDs that result from SEEK's in-built search function
    :param return_unique:
    """
    results_dict = {}
    list_of_ids = []
    list_of_titles = []
    asset_type = 'data_files'
    search_results = search(searchterm, server_dict, asset_type, verbose)
    if search_results:
        for file in search_results['data']:
            file_title = file['attributes']['title']
            file_id = file['id']
            if return_unique and file_title in list_of_titles:#only store id of most recent file (highest id)
                print(f"found file with title {file_title} again. Consider removing it")
                #find position of title in list
                position = list_of_titles.index(file_title)
                current_id = list_of_ids[position]
                #replace if newer
                if int(file_id) > int(current_id):
                    list_of_ids[position] = file_id
                    del results_dict[current_id]
                    results_dict[file_id] = file_title
            else:
                list_of_ids.append(file_id)
                list_of_titles.append(file_title)
                results_dict[file_id] = file_title
            if verbose:
                print(f"Term {searchterm} found in file {file_id}: {file_title}")
        if verbose:
            print(f"{len(list_of_ids)} file IDs searchterm '{searchterm}': {list_of_ids}")
    # return list_of_ids, list_of_titles
    return results_dict, list_of_ids


def return_ids_and_titles_from_tag_search(searchtag, server_dict, verbose):
    """returns list of file IDs that have the searched tag"""
    results_dict = {}
    list_of_ids = []
    # search for searchtag in data files
    searchtype = 'data_files'
    search_results = search(searchtag, server_dict, searchtype, verbose)
    if search_results:
        # fetch data files resulting from search results
        session = hf.create_session(server_dict['auth_dict'])
        base_url = server_dict['base_url']
        for file in search_results['data']:
            file_id = file['id']
            data_file = session.get(f'{base_url}/data_files/{file_id}',
                                    verify=server_dict['working_certificate']).json()
            # only choose tags that exactly match the searchtag
            file_tags = data_file['data']['attributes']['tags']
            if file_tags:
                for tag in file_tags:
                    if searchtag.upper() == tag.upper():
                        list_of_ids.append(file['id'])
                        results_dict[file['id']] = file['attributes']['title']
                        if verbose:
                            print(f"File {file_id} has tag {tag}")
            elif verbose:
                print(f"File {file_id} has no tags")
        if verbose:
            print(f"{len(list_of_ids)} file IDs with tag '{searchtag}': {list_of_ids}")
    # return list_of_ids
    return results_dict, list_of_ids


def return_ids_and_titles_from_title_search(searchterm, server_dict, verbose):
    """returns list of IDs of files that contain the equivalent 00x format of the searchterm in their title
    example: searchterm 'tm3' looks for 'tm003' in title"""
    results_dict = {}
    list_of_ids = []
    searchterm_extended = hf.add_leading_zeros(searchterm)
    # fetch all data files
    session = hf.create_session(server_dict['auth_dict'])
    base_url = server_dict['base_url']
    list_of_all_files = session.get(base_url + '/data_files', verify=server_dict['working_certificate']).json()['data']
    # iterate over files and check if searchterm in title
    for file in list_of_all_files:
        title = file['attributes']['title']
        if (searchterm.upper() or searchterm_extended.upper()) in title.upper():
            list_of_ids.append(file['id'])
            results_dict[file['id']] = file['attributes']['title']
            if verbose:
                print(f"Term {searchterm} found in file {file['id']}: {title}")
    if verbose:
        print(f"{len(list_of_ids)} file IDs with titles matching the term '{searchterm}': {list_of_ids}")
    # return list_of_ids
    return results_dict, list_of_ids



def fuzzy_search(searchterm, session, base_url):
    """returns list of files metadata that match fuzzy search"""
    headers = {"Content-type": "application/vnd.api+json",
               "Accept": "application/vnd.api+json",
               "Accept-Charset": "ISO-8859-1"}
    searchterm = f'{searchterm}~'
    payload = {'q': searchterm}
    r = session.get(base_url + '/search', headers=headers, params=payload)

    return r.json()['data']


def search(searchterm, server_dict, searchtype='', verbose=False):
    if searchtype == '':
        payload = {'q': searchterm}
    else:
        payload = {'q': searchterm,
                   'search_type': searchtype}  # types: data_files, assays, investigations, people,...
    base_url = server_dict['base_url']
    session = hf.create_session(server_dict['auth_dict'])
    r = session.get(base_url + '/search', params=payload,
                    verify=server_dict['working_certificate'])
    if verbose:
        print(f"{len(r.json()['data'])} {searchtype} found for '{searchterm}'")
    return r.json()

