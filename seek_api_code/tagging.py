from seek_api_code import helper_functions as hf


def mass_tagging(server_dict, keyword_tag_tuple_list, testing=True, raise_error=False, verbose=False):
    """unsuccessful tagging most likely based on missing manage rights"""
    # list_of_matching_files = return_list_of_matching_files(base_url, username, password, searchterm, keyword)
    base_url = server_dict['base_url']
    session = hf.create_session(server_dict['auth_dict'])
    list_of_all_files = session.get(base_url + '/data_files', verify=server_dict['working_certificate']).json()[
        'data']
    tagging_dict = {}
    for keyword, tag in keyword_tag_tuple_list:
        print(f'searching for files with keyword: {keyword}')
        successfully_tagged, unsuccessfully_tagged = tag_all(server_dict, list_of_all_files,
                                                             keyword, tag, testing, raise_error, verbose)
        tagging_dict[keyword] = {}
        tagging_dict[keyword]['successfully_tagged'] = successfully_tagged
        tagging_dict[keyword]['unsuccessfully_tagged'] = unsuccessfully_tagged
    return tagging_dict


def mass_tag_removal(server_dict, tag_regex_list, testing=True, raise_error=False, verbose=False):
    """remove tags according to regex list"""
    base_url = server_dict['base_url']
    session = hf.create_session(server_dict['auth_dict'])

    list_of_all_files = session.get(base_url + '/data_files', verify=server_dict['working_certificate']).json()['data']

    for file in list_of_all_files:
        title = file['attributes']['title']
        file_id = file['id']
        link_to_file = base_url + f'/data_files/{file_id}'
        r = session.get(link_to_file, verify=server_dict['working_certificate'])
        response_json = r.json()
        list_of_tags = response_json['data']['attributes']['tags']
        need_to_patch = False
        if list_of_tags:  # checks if there are existing tags
            for tag in tag_regex_list:
                list_of_tags_copy = list_of_tags.copy()
                for existing_tag in list_of_tags_copy:  # checks all filetags
                    regex_tag_match = hf.match_regex(existing_tag, tag, verbose)  # if the regextag matches the file tag
                    if regex_tag_match:
                        list_of_tags.remove(existing_tag)
                        need_to_patch = True
                        if verbose:
                            print(f'removing tag: {existing_tag} from file {title}')
        if need_to_patch:
            response_json['data']['attributes']['tags'] = list_of_tags
            print(f'{base_url}/data_files/{file_id}')
            if testing:
                print('just testing tag removal')
            else:
                response = session.patch(f'{base_url}/data_files/{file_id}', json=response_json,
                                         verify=server_dict['working_certificate'])
                if response.status_code == 200:
                    print('update successful')
                else:
                    if raise_error:
                        print(response.raise_for_status())
                    else:
                        print(f'silently passed error: {response.status_code}  for file: {title}')


def tag_all(server_dict, search_results, keyword, tag, testing, raise_error=False, verbose=False):
    list_of_matching_files = return_files_to_tag(search_results, keyword, verbose)
    successfully_tagged = []
    unsuccessfully_tagged = []
    if testing:
        print('not tagging yet, only testing')
        # print(f'{list_of_matching_files}')
    else:
        for i in range(len(list_of_matching_files)):
            match_dict = list_of_matching_files[i]
            file_id = match_dict['entry']['id']
            tag_list = match_dict['tags_to_use']
            success, title = add_tags_to_file(file_id, tag_list, server_dict, raise_error, verbose)
            if success:
                successfully_tagged.append(title)
            else:
                unsuccessfully_tagged.append(title)

    return successfully_tagged, unsuccessfully_tagged


def add_tags_to_file(file_id, tag_list, server_dict, raise_error=True, verbose=False, ):
    base_url = server_dict['base_url']
    print(f'Adding tag \"{tag_list}\" to file {file_id}')
    # creating session
    session = hf.create_session(server_dict['auth_dict'])
    # accessing file
    link_to_file = base_url + f'/data_files/{file_id}'
    r = session.get(link_to_file, verify=server_dict['working_certificate'])
    if r.status_code == 404:
        print(f'failed to reach {link_to_file}')
    print(f"Existing Tags: {r.json()['data']['attributes']['tags']}")
    # copying current metadata
    payload = r.json()
    # adding tag
    if r.json()['data']['attributes']['tags']:
        for tag in tag_list:
            if tag not in r.json()['data']['attributes']['tags']:
                payload['data']['attributes']['tags'].append(tag)
    else:
        if verbose:
            print('no tags existing, creating tag list')
            print(f"url: {payload['data']['attributes']['versions'][-1]['url']}")
        payload['data']['attributes']['tags'] = tag_list

    # try:
    answer = session.patch(f'{base_url}/data_files/{file_id}', json=payload, verify=server_dict['working_certificate'])
    if answer.status_code == 200:
        print('update successful')
        return True, payload['data']['attributes']['title']
    else:
        if raise_error:
            print(answer.raise_for_status())
        else:
            print(f'silently passed error: {answer.status_code} ')
            return False, payload['data']['attributes']['title']
    # except HTTPError:
    #    print("error")



def return_files_to_tag(search_results, keyword, verbose=False):
    """filters search results for keyword"""
    filtered_list = []
    for entry in search_results:
        entry_type = entry['type']
        if entry_type == 'data_files':
            regex_match = hf.keyword_in_title_regex(entry, keyword)
            if regex_match:
                if verbose:
                    print(f"file \"{entry['attributes']['title']}\" matched by \"{regex_match}\"")
                match_dict = return_tags_to_use(entry, regex_match, verbose)
                filtered_list.append(match_dict)
    return filtered_list


def return_tags_to_use(entry, regex_match, verbose=False):
    additional_tag = hf.remove_leading_zeros(regex_match)
    regex_matches_list = [regex_match]
    if (additional_tag):
        regex_matches_list.append(additional_tag)
    # if verbose:
    print(f"tagging {entry['attributes']['title']} with: {regex_matches_list}")
    tags_dict = {'entry': entry, 'tags_to_use': regex_matches_list}
    return tags_dict

