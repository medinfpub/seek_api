#import seek_api.helper_functions as hf
from seek_api_code import helper_functions as hf
import os
from time import sleep
import json

import copy
def create_investigation_json(title, associated_project_id, associated_programme_id, manager_ids,
                              viewable_by_programme=True,
                              description=''):
    """create the json for an investigation to be uploaded to seek"""
    investigation_json = {
        'data': {
            'type': 'investigations',
            'attributes': {
                'title': title,
                'description': description,
                'other_creators': None,
                'policy': {
                    'access': 'no_access',
                    'permissions': []
                }
            },
            'relationships': {
                'creators': {'data': []},
                'projects': {'data': [{'id': associated_project_id, 'type': 'projects'}]},
                'programmes': {'data': [{'id': associated_programme_id, 'type': 'programmes'}]},
            }
        }
    }

    for manager_id in manager_ids:
        investigation_json['data']['attributes']['policy']['permissions'].append(
            {'resource': {'id': manager_id, 'type': 'people'}, 'access': 'manage'})
        investigation_json['data']['relationships']['creators']['data'].append(
            {'id': manager_id, 'type': 'people'})

    if viewable_by_programme:
        investigation_json['data']['attributes']['policy']['permissions'].append(
            {'resource': {'id': associated_programme_id, 'type': 'programmes'},
             'access': 'view'}
        )

    return investigation_json


def post_investigation(title, associated_programme_id, associated_project_id, server_dict, manager_ids,
                       verbose=False, viewable_by_programme=True):
    """upload the create and upload json to seek"""

    if verbose:
        print(
            f"""Creating Investigation \'{title}\' 
        in Project: {hf.get_title_of_object(server_dict, associated_project_id, 'project')} """
        )

    investigation_json = create_investigation_json(title, associated_project_id, associated_programme_id, manager_ids,
                                                   viewable_by_programme)

    base_url = server_dict['base_url']
    session = hf.create_session(server_dict['auth_dict'])
    investigation_response = session.post(base_url + '/investigations', json=investigation_json,
                                          verify=server_dict['working_certificate'])
    if (investigation_response.status_code != 200):
        print(
            f'response of posting investigation: {investigation_response.json()} \n possible error: "must be associated with one of your projects." this means submitter must be member of project. it is not enough to have manage rights')

    investigation_response.raise_for_status()

    investigation_id = investigation_response.json()['data']['id']
    return investigation_id


def create_study_json(title, associated_programme_id, associated_investigation_id, manager_ids):
    """
    Provides the json, that is necessary to create a study
            Parameters:
                    title (str): Title that should be given to study
                    associated_investigation_id (int): The id of the investigation that the study should be linked to
                    associated_programme_id (int): The id of the programme that the investigation can be viewed by
                    manager_ids (list): Ids of people who should have manage rights
            Returns:
                    study_meta_data (dict): all meta-information that can be given to seek when creating a study
    """

    study_json = {
        "data": {
            "type": "studies",
            "attributes": {
                "title": title,
                "description": "",
                "policy": {
                    "access": "no_access",
                    "permissions": [
                        {
                            "resource": {"id": associated_programme_id,
                                         "type": "programmes"},
                            "access": "view"},
                    ]
                }
            },
            "relationships": {
                'creators': {'data': []},
                "investigation": {"data": {"id": associated_investigation_id, "type": "investigations"}},
                'programmes': {'data': [{'id': associated_programme_id, 'type': 'programmes'}]},
                "publications": {"data": []},
            }
        }
    }

    for manager_id in manager_ids:
        study_json['data']['attributes']['policy']['permissions'].append(
            {'resource': {'id': manager_id, 'type': 'people'}, 'access': 'manage'})
        study_json['data']['relationships']['creators']['data'].append(
            {'id': manager_id, 'type': 'people'})
    return study_json


def post_study(title, associated_programme_id, associated_investigation_id, server_dict, manager_ids, verbose=False):
    if verbose:
        print(
            f"""Creating study \'{title}\' in Investigation {hf.get_title_of_object(server_dict, associated_investigation_id, 'investigation', verbose)}""")

    study_json = create_study_json(title, associated_programme_id, associated_investigation_id, manager_ids)

    base_url = server_dict['base_url']
    session = hf.create_session(server_dict['auth_dict'])
    study_response = session.post(base_url + '/studies', json=study_json, verify=server_dict['working_certificate'])
    while study_response.status_code != 200:
        print(
            f'response of posting study: {study_response.json()}')  # possible error: 'must be associated with one of your <p>project</p>s.'. this means submitter must be member of project. it is not enough to have manage rights
        print('sleeping and trying again')
        sleep(2)
        study_response = session.post(base_url + '/studies', json=study_json, verify=server_dict['working_certificate'])

    study_response.raise_for_status()

    study_id = study_response.json()['data']['id']
    return study_id


def create_assay_json(title, associated_study_id, associated_investigation_id, associated_project_id,
                      associated_programme_id, manager_ids, description=''):
    """Creates json that contains all meta-information necessary to create an assay in seek
    description is optional
    """

    assay_json = {'data': {'type': 'assays',
                           'attributes': {'policy': {'access': 'no_access',
                                                     'permissions': [
                                                         {'resource': {'id': associated_project_id, 'type': 'projects'},
                                                          'access': 'view'},
                                                         {'resource': {'id': associated_programme_id,
                                                                       'type': 'programmes'}, 'access': 'view'}]},
                                          'title': title,
                                          'description': description,
                                          'assay_class': {'title': 'Experimental assay',
                                                          'key': 'EXP',
                                                          },
                                          },
                           'relationships': {
                               'creators': {'data': []},
                               'projects': {'data': [{'id': associated_project_id, 'type': 'projects'}]},
                               'investigation': {'data': {'id': associated_investigation_id, 'type': 'investigations'}},
                               'study': {'data': {'id': associated_study_id, 'type': 'studies'}},
                               'documents': {'data': []}}},
                  }

    for manager_id in manager_ids:
        assay_json['data']['attributes']['policy']['permissions'].append(
            {'resource': {'id': manager_id, 'type': 'people'}, 'access': 'manage'})
        assay_json['data']['relationships']['creators']['data'].append(
            {'id': manager_id, 'type': 'people'})

    return assay_json


def post_assay(title, associated_programme_id, associated_project_id, associated_investigation_id, associated_study_id, server_dict, manager_ids,
               verbose=False):
    """
    Posts an assay on the server
            Parameters:
                    title (str): Title that should be given to the assay
                    associated_study_id (int): The id of the study that the assay should be linked to
                    associated_investigation_id (int): The id of the investigation that the study is linked to
                    associated_project_id (int): The id of the project that the investigation is linked to
                    associated_programme_id (int): The id of the programme that the project is linked to
                    server_dict (dict): information about and credentials for the server
                    manager_ids (list): list of id's of people who should get manage rights
                    verbose (bool): print detailed information
            Returns:
                    assay_id (dict): id of the posted assay generated by seek
    """
    assay_json = create_assay_json(title, associated_study_id, associated_investigation_id, associated_project_id,
                                   associated_programme_id, manager_ids)
    if verbose:
        print(
            f"Creating assay \'{title}\' with associated investigation: {hf.get_title_of_object(server_dict, associated_investigation_id, 'investigation')}"
            f"and associated study: {hf.get_title_of_object(server_dict, associated_study_id, 'study')}"
        )
    base_url = server_dict['base_url']
    session = hf.create_session(server_dict['auth_dict'])
    response = session.post(base_url + '/assays', json=assay_json,
                            verify=server_dict['working_certificate'])
    while response.status_code != 200:
        print(f'failed with status code {response.status_code}')
        if (response.status_code != 500):
            print(f'response of posting assay: {response.json()}')
        print('sleeping and trying again')
        sleep(2)
        response = session.post(base_url + '/assays', json=assay_json,
                                verify=server_dict['working_certificate'])
    response.raise_for_status()

    assay_id = response.json()['data']['id']

    return assay_id


def create_data_file_json(title, associated_programme_id, associated_project_id, associated_assay_ids, manager_ids):
    """Creates json that contains all meta-information necessary to create a datafile in seek"""

    data_file_json = {
        'data': {
            'type': 'data_files',
            'attributes': {'policy':
                               {'access': 'no_access',
                                'permissions': [
                                    {'resource': {'id': associated_project_id,
                                                  'type': 'projects'}, 'access': 'download'},  # manage
                                    {'resource': {'id': associated_programme_id,
                                                  'type': 'programmes'}, 'access': 'download'},
                                ]},
                           'title': title,
                           'license': 'CC-BY-4.0',
                           },
            'relationships': {

                'projects': {'data': [{'id': associated_project_id, 'type': 'projects'}]},
                'assays': {'data': []},
                'creators': {'data': []}
            },
        }
    }

    for associated_assay_id in associated_assay_ids:
        data_file_json['data']['relationships']['assays']['data'].append({'id': associated_assay_id, 'type': 'assays'})

    for creator_id in manager_ids:
        data_file_json['data']['relationships']['creators']['data'].append({'id': creator_id, 'type': 'people'})
        data_file_json['data']['attributes']['policy']['permissions'].append(
            {'resource': {'id': creator_id, 'type': 'people'}, 'access': 'manage'})

    return data_file_json


#def post_data_file_from_json(server_dict):

def post_file(server_dict, absolute_filepath, title, associated_project_id, associated_programme_id, associated_assay_ids, manager_ids, verbose=False):
    base_url = server_dict['base_url']
    session = hf.create_session(server_dict['auth_dict'])
    if verbose:
        #r_study = session.get(f'{base_url}/studies/{associated_study_id}', verify=server_dict['working_certificate'])
        #print(f"\tdetermined study:  {r_study.json()['data']['attributes']['title']}")
        for associated_assay_id in associated_assay_ids:
            r_assay = session.get(f'{base_url}/assays/{associated_assay_id}', verify=server_dict['working_certificate'])
            print(f"\tdetermined assay:  {r_assay.json()['data']['attributes']['title']}")
    data_file_json = create_data_file_json(title, associated_programme_id, associated_project_id, associated_assay_ids,
                                           manager_ids)
    file_type = hf.get_mime_type(absolute_filepath)
    data_file_json['data']['attributes']['content_blobs'] = [{'original_filename': os.path.basename(absolute_filepath),
                                                              'content_type': file_type}]

    link, data_file_id = upload_metadata(data_file_json, base_url, server_dict, session, verbose=verbose)
    if link:
        # GET link to future data file location
        if verbose:
            link_to_metadata = link.split('/content_blobs')[0]
            print(f"\tGET request target: {link_to_metadata}")
            meta_data_response = session.get(link_to_metadata, verify=server_dict['working_certificate'])
            if meta_data_response.status_code == 403:
                print(f"\tGET request: {meta_data_response}")

        upload_object = upload_file(link, absolute_filepath, session, server_dict, verbose)

        if upload_object.status_code == 200:
            if verbose:
                print('\t\tsuccessfully uploaded file')
            return data_file_id
        else:
            if verbose:
                print(f'response from unsuccessfully uploading object: {upload_object.status_code}')
                print(f'{upload_object.json()}')
                print('sleeping')
            sleep(2)
            if verbose:
                print('and trying again')
            upload_object = upload_file(link, absolute_filepath, session, server_dict, verbose)
            upload_object.raise_for_status()
            if upload_object.status_code == 200:
                if verbose:
                    print('\t\tsuccessfully uploaded file')
                return data_file_id
            else:
                return False
    else:
        print('no upload link generated')
        return False


def upload_file(blob_url, local_filepath, session, server_dict, verbose=False):
    """Extract the URL for the local data"""
    #local_file = open(local_filepath, mode='rb')
    local_file = {'file': open(local_filepath, 'rb')}
    if verbose:
        print(f'\t\tuploading {local_filepath}')  # to {blob_url}')
    #content = local_file.read()
    #upload_object = session.put(blob_url, data=content, headers={'Content-Type': 'application/octet-stream'}, verify=server_dict['working_certificate'])
    upload_object = session.put(blob_url, files=local_file, verify=server_dict['working_certificate'])

    return upload_object


def upload_metadata(data_file_json, base_url, server_dict, session, verbose=False):

    r = session.post(base_url + '/data_files', json=data_file_json, verify=server_dict['working_certificate'])
    if verbose:
        print(f'\t\tresponse from uploading metadata {r.status_code}')  # r.raise_for_status()
    while r.status_code != 200:
        if verbose:
            try:
                print(f'response to posting metadata: {r.json()}')
            except json.JSONDecodeError:
                    print(f'unable to decode json got following object: {r}')

            print('sleeping and trying to upload metadata again')
        sleep(5)
        r = session.post(base_url + '/data_files', json=data_file_json, verify=server_dict['working_certificate'])

    server_json = r.json()
    # print(r.json())

    if verbose:
        print(f"\t\tcreated metadata at {server_json['data']['attributes']['versions'][-1]['url']}")
        print(f"\t\treturning link for content blob: {server_json['data']['attributes']['content_blobs'][0]['link']}")
    blob_url = server_json['data']['attributes']['content_blobs'][0]['link']
    data_file_id = server_json['data']['id']
    return blob_url, data_file_id






def upload_from_folder_structure_to_investigation(folder_structure_dict, server_dict, associated_programme_id, associated_project_id,
                                                  manager_ids, allow_duplicates=False):
    """upload from a folder structure given a corresponding isa structure in seek
    writes 2 logfiles with the paths of successful and unsuccessful uploads"""
    investigation_folder_name = folder_structure_dict['investigation']['title']
    folder_path_absolute = os.path.join(os.path.dirname(os.path.dirname((os.path.dirname(os.getcwd())))),'data', investigation_folder_name)
    print(f'folder contains {os.listdir(folder_path_absolute)}')

    associated_investigation_id = folder_structure_dict['investigation']['id']
    print(f'uploading to {investigation_folder_name} with id {associated_investigation_id}')
    successfully_uploaded_files = []
    unsuccessfully_uploaded_files = []
    for study_folder_name in os.listdir(folder_path_absolute):
        associated_study_id = folder_structure_dict['studies'][study_folder_name]['id'] #here we abandonded the 'title' structure -> fix
        print(f"{study_folder_name} with id {associated_study_id}")
        for assay_folder_name in os.listdir(os.path.join(folder_path_absolute, study_folder_name)):
            associated_assay_ids = [
                folder_structure_dict['studies'][study_folder_name]['assays'][assay_folder_name]['id']]
            print(f"\t{assay_folder_name} with id {associated_assay_ids}")
            for file_name in os.listdir(os.path.join(folder_path_absolute, study_folder_name, assay_folder_name)):
                files_path_absolute = os.path.join(folder_path_absolute, study_folder_name, assay_folder_name)
                print(hf.color.BOLD + f'file name: {file_name}' + hf.color.END)
                absolute_filepath = os.path.join(files_path_absolute, file_name)
                if hf.check_if_file_exists(file_name, server_dict) and not allow_duplicates:
                    print(f'{file_name} already in seek')
                    unsuccessfully_uploaded_files.append(absolute_filepath)
                else:
                    data_file_id = post_file(server_dict, absolute_filepath, file_name, associated_project_id,
                                             associated_programme_id, associated_assay_ids, manager_ids, verbose=True)
                    if (data_file_id):
                        successfully_uploaded_files.append(absolute_filepath)
                    else:
                        unsuccessfully_uploaded_files.append(absolute_filepath)
                    # upload_object.raise_for_status()
    return successfully_uploaded_files, unsuccessfully_uploaded_files





def create_investigation_from_folder_structure(investigation_folder_name, server_dict, associated_programme_id,
                                               associated_project_id, manager_ids, verbose=False,
                                               viewable_by_programme=True):
    """
    upload creating a new structure
    :param investigation_folder_name:
    :param server_dict:
    :param associated_programme_id:
    :param associated_project_id:
    :param manager_ids:
    :param verbose:
    :param viewable_by_programme:
    :return:
    """
    files_path_absolute = os.path.join(os.getcwd(), investigation_folder_name)
    print(f'folder contains {os.listdir(files_path_absolute)}')
    created_objects = {}
    associated_investigation_id = post_investigation(investigation_folder_name, associated_programme_id,
                                                     associated_project_id, server_dict, manager_ids, verbose,
                                                     viewable_by_programme)
    created_objects['investigation'] = {'title': investigation_folder_name, 'id': associated_investigation_id}
    created_objects['studies'] = {}
    for study_folder_name in os.listdir(files_path_absolute):
        study_id = post_study(study_folder_name, associated_programme_id, associated_investigation_id, server_dict,
                              manager_ids, verbose)
        created_objects['studies'][study_folder_name] = {'id': study_id, 'assays': {}}
        print(f'\tfolder {study_folder_name} contains {os.listdir(files_path_absolute + "/" + study_folder_name)}')
        associated_study_id = study_id
        for assay_folder_name in os.listdir(files_path_absolute + "/" + study_folder_name):
            assay_id = post_assay(assay_folder_name, associated_programme_id, associated_project_id, associated_investigation_id, associated_study_id,
                                  server_dict, manager_ids, verbose)
            created_objects['studies'][study_folder_name]['assays'][assay_folder_name] = {'id': assay_id}
    return created_objects



def post_asset_from_json(structure_and_metadata_dict, server_dict, session, asset_type, asset_id):
    base_url = server_dict['base_url']
    asset_json = {'data':structure_and_metadata_dict[asset_type][asset_id]}
    title = asset_json['data']['attributes']['title']
    del asset_json['data']['id']
    print(f'uploading {hf.singular_of(asset_type)} {title}')
    server_response = session.post(base_url + '/' + asset_type, json=asset_json,
                                          verify=server_dict['working_certificate'])
    return server_response
#def post_asset_from_json(asset_folder_name, associated_programme_id, associated_project_id, server_dict):


def upload_from_json(root_folder, server_dict, structure_and_metadata_dict, new_programme_id, new_project_id, dry_run, verbose=True):

    """
    uploads an investigation based on the json of a previously downloaded investigation
    :param root_folder: folder of the investigation
    :param server_dict: contains adress and credentials to server that we want to upload to
    :param new_programme_id: id of SEEK programme, the investigation will be associated with
    :param associated_project_id: id of SEEK project, the investigation will be associated with
    :param dry_run:if True, only tells what it WOULD upload, but doesnt actually upload it
    :param verbose: if True, gives a lot of output in console
    :return: successfully_uploaded_files, unsuccessfully_uploaded_files: lists to check what has actually been uploaded
    """
    print(f'uploading to {server_dict["base_url"]}')
    new_structure_and_metadata_dict = copy.deepcopy(structure_and_metadata_dict)#{}
    session = hf.create_session(server_dict['auth_dict'])
    # loop over dict
    for asset_type, assets_dicts in structure_and_metadata_dict.items():#iterate over asset types
        #new_structure_and_metadata_dict[asset_type] = {}
        for asset_id, asset_dict in assets_dicts.items(): #iterate over assets of one type
            asset_name = asset_dict['attributes']['title']
            if asset_type != 'data_files':
                if asset_type == "investigations": #manually add new_project_id to investigation(s)
                    new_structure_and_metadata_dict[asset_type][asset_id]['relationships']['projects']['data'][0]['id'] = new_project_id
                if verbose:
                    print(f'\nfolder {asset_name} contains {len(asset_dict["relationships"][hf.next_hierarchy[asset_type]]["data"])} studies')
                if not dry_run:
                    #for asset_dicts in asset_dict["relationships"][hf.next_hierarchy[asset_type]]["data"]:
                    server_response = post_asset_from_json(new_structure_and_metadata_dict, server_dict, session, asset_type, asset_id)
                    new_asset_id = server_response.json()['data']['id']
                    new_structure_and_metadata_dict[asset_type][new_asset_id] = asset_dict
                    for lower_level_id, lower_level_asset in structure_and_metadata_dict[hf.next_hierarchy[asset_type]].items():
                        if asset_type == 'assays':
                            new_structure_and_metadata_dict[hf.next_hierarchy[asset_type]][lower_level_id]['relationships'][change_to_singluar_sometimes(asset_type)]['data'][0]['id'] = new_asset_id# erstmal einfach den ersten assay auswählen (ein data file kann theoretisch mehreren assays zugeordnet werden)
                        else:
                            new_structure_and_metadata_dict[hf.next_hierarchy[asset_type]][lower_level_id]['relationships'][change_to_singluar_sometimes(asset_type)]['data']['id'] = new_asset_id
            else:
                server_response = post_asset_from_json(new_structure_and_metadata_dict, server_dict, session, asset_type, asset_id)
                while server_response.status_code != 200:
                    if verbose:
                        try:
                            print(f'response to posting metadata: {server_response.json()}')
                        except json.JSONDecodeError:
                            print(f'unable to decode json got following object: {server_response}')

                        print('sleeping and trying to upload metadata again')
                    sleep(4)
                    server_response = post_asset_from_json(new_structure_and_metadata_dict, server_dict, session, asset_type, asset_id)

                server_json = server_response.json()
                blob_url = server_json['data']['attributes']['content_blobs'][0]['link']
                data_file_id = server_json['data']['id']
                absolute_filepath = structure_and_metadata_dict[asset_type][asset_id]['path']
                upload_object = upload_file(blob_url, absolute_filepath, session, server_dict, verbose)
                while upload_object.status_code != 200:
                    upload_object = upload_file(blob_url, absolute_filepath, session, server_dict, verbose)

def change_to_singluar_sometimes(asset_type):
    """
    plural and singular are not consistent in relationships in json from server
    :param asset_type:
    :return: how asset_type is stored in relationships of child_asset
    """
    if asset_type == 'investigations':
        return 'investigation'
    elif asset_type == 'studies':
        return 'study'
    elif asset_type == 'assays':
        return 'assays'


def upload_from_folder_structure_to_investigation_flexible(folder_path_absolute, server_dict, associated_programme_id, folder_structure_dict,
                                                           associated_project_id, manager_ids, dry_run, verbose=True, viewable_by_programme=True):
    """uploads from a folder structure given a corresponding isa structure in seek
    creates assets if they dont exist
    writes 2 logfiles with the paths of successful and unsuccessful uploads
    allows to upload data files that already exist in the project, but not in the same assay
    :param folder_path_absolute: path to your local folder that is supposed to be uploaded to SEEK 'as an investigation'
    :param server_dict: contains adress and credentials to server that we want to upload to
    :param folder_structure_dict: folder structure of files that are supposed to be uploaded. if subfolders are not specified. all subfolders will be uploaded
    :param associated_programme_id: id of SEEK programme, the investigation will be associated with
    :param  associated_project_id: id of SEEK project, the investigation will be associated with
    :param dry_run:if True, only tells what it WOULD upload, but doesnt actually upload it
    :param viewable_by_programme:
    :param verbose: if True, gives a lot of output in console"""
    investigation_folder_name = folder_structure_dict['investigations']['title']
    if verbose:
        print(f'\nfolder {folder_path_absolute} contains {os.listdir(folder_path_absolute)}')
    if not dry_run:
        associated_investigation_id = create_asset_if_missing(associated_programme_id, associated_project_id, folder_structure_dict,
                                                              investigation_folder_name, manager_ids, server_dict, 'investigations', verbose,
                                                              viewable_by_programme)
    successfully_uploaded_files = []
    unsuccessfully_uploaded_files = []
    for study_folder_name in os.listdir(folder_path_absolute):
        if study_folder_name not in folder_structure_dict['studies']:
            continue
        if dry_run:
            print(f'study: {study_folder_name}\n')
        else:
            associated_study_id = create_asset_if_missing(associated_programme_id, associated_project_id, folder_structure_dict, study_folder_name,
                                                          manager_ids, server_dict, 'studies', verbose, viewable_by_programme,
                                                          associated_investigation_id=associated_investigation_id)

        for assay_folder_name in os.listdir(os.path.join(folder_path_absolute, study_folder_name)):
            if dry_run:
                print(f'\tassay: {assay_folder_name}\n')
            else:
                associated_assay_ids = [ #in principle a data file can be associate with assays. we use that in the kfo5002
                    create_asset_if_missing(associated_programme_id, associated_project_id, folder_structure_dict, assay_folder_name, manager_ids,
                                            server_dict, 'assays', verbose, viewable_by_programme,
                                            associated_investigation_id=associated_investigation_id, associated_study_id=associated_study_id)
                    ]
                print(f"\t{assay_folder_name} with id {associated_assay_ids}")
            for file_name in os.listdir(os.path.join(folder_path_absolute, study_folder_name, assay_folder_name)):
                if dry_run:
                    print(f'\t\tfile: {file_name}\n')
                else:
                    files_path_absolute = os.path.join(folder_path_absolute, study_folder_name, assay_folder_name)
                    print(hf.color.BOLD + f'file name: {file_name}' + hf.color.END)
                    absolute_filepath = os.path.join(files_path_absolute, file_name)
                    if hf.check_if_file_exists_in_associated_assays(file_name, associated_assay_ids, server_dict):# and not allow_duplicates:
                        print(f'{file_name} already in assay')
                        unsuccessfully_uploaded_files.append(absolute_filepath)
                    else:
                        data_file_id = post_file(server_dict, absolute_filepath, file_name, associated_project_id,
                                                 associated_programme_id, associated_assay_ids, manager_ids, verbose=True)
                        if (data_file_id):
                            successfully_uploaded_files.append(absolute_filepath)
                        else:
                            unsuccessfully_uploaded_files.append(absolute_filepath)
                        # upload_object.raise_for_status()
    return successfully_uploaded_files, unsuccessfully_uploaded_files


def create_asset_if_missing(associated_programme_id, associated_project_id, folder_structure_dict, asset_folder_name, manager_ids, server_dict,
                            asset_type_to_create, verbose, viewable_by_programme, associated_investigation_id=False, associated_study_id=False,
                            study_folder_name=False):


    try:
        # check for asset id supplied by folder_structure_dict
        if asset_type_to_create == 'investigations':
            associated_asset_id = folder_structure_dict['investigations']['id']
        elif asset_type_to_create == 'studies':
            # for study:
            associated_asset_id = folder_structure_dict['studies'][asset_folder_name]['id']  # here we abandonded the 'title' structure -> fix
        elif asset_type_to_create == 'assays':
            associated_asset_id = folder_structure_dict['studies'][study_folder_name]['assays'][asset_folder_name]['id']

        print(f'uploading to {asset_folder_name} with id {associated_asset_id}')
    except KeyError:

        # check if investigation with name is already attached to project
        link_to_asset = f'{server_dict["base_url"]}/projects/{associated_project_id}'
        session = hf.create_session(server_dict['auth_dict'])
        asset_response = session.get(link_to_asset, verify=server_dict['working_certificate'])
        associated_asset_id = False
        if asset_type_to_create in asset_response.json()['data']['relationships']:
            if verbose:
                print(f"found {asset_response.json()['data']['relationships'][asset_type_to_create]['data']} already associated with the provided project")
            for asset in asset_response.json()['data']['relationships'][asset_type_to_create]['data']:
                if hf.get_title_of_object(server_dict, asset["id"], asset_type_to_create, verbose=False) == asset_folder_name:
                    associated_asset_id = asset["id"]

        # if there is no investigation with name from folder structure in seek: create one
        if not associated_asset_id:
            print(f'creating {asset_type_to_create}')
            created_objects = {}
            if asset_type_to_create == 'investigations':
                associated_asset_id = post_investigation(asset_folder_name, associated_programme_id,
                                                             associated_project_id, server_dict, manager_ids, verbose,
                                                             viewable_by_programme)
            elif asset_type_to_create == 'studies':
                associated_asset_id = post_study(asset_folder_name, associated_programme_id, associated_investigation_id, server_dict, manager_ids, verbose=False)
            elif asset_type_to_create == 'assays':
                associated_asset_id = post_assay(asset_folder_name, associated_programme_id, associated_project_id, associated_investigation_id,
                                                 associated_study_id, server_dict, manager_ids, verbose=False)
            #created_objects[asset_type] = {'title': asset_folder_name, 'id': associated_asset_id}
            print(f"created {asset_folder_name} with id {associated_asset_id}")
        else:
            print(f'{asset_type_to_create} already exists')
    return associated_asset_id
