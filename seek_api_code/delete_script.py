from seek_api_code import deletion as d
from seek_api_code import servers as s

server_dict = s.official_testing_server_dict

asset_type = 'investigation'
asset_id = 397
not_to_delete = []#[('investigations', '9'), ('studies', '18'), ('assays', '45'), ('data_files', '1673')]

d.recursively_delete_asset(server_dict, asset_id, asset_type, delete_itself=True, verbose=True,
                           exceptions=not_to_delete, dry_run=False)