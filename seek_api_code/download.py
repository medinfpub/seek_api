from seek_api_code import helper_functions as hf
import os
import searching as s



def bulk_download(searchterm, server_dict, subfolder, return_unique, searchmethod='tag_search', verbose=False):
    """download all files according to the given searchmethod
    :param return_unique:
    """
    if searchmethod == 'tag_search':
        # all files that have the searchtag
        results_dict, file_ids = s.return_ids_and_titles_from_tag_search(searchterm, server_dict, verbose)
    elif searchmethod == 'title_search':
        # all files whose title contains the searchterm or its 3-digit-format (00x)
        #     e.g. searchterm can be tm3/tm03/tm003 or 7t/07t/007t
        results_dict, file_ids = s.return_ids_and_titles_from_title_search(searchterm, server_dict, verbose)
    elif searchmethod == 'seek_search':
        # all files corresponding to the built-in seek search
        results_dict, file_ids = s.return_ids_and_titles_from_seek_search(searchterm, return_unique, server_dict, verbose)
    else:
        print('select search method from: [tag_search, title_search, seek_search]')
    download_from_ids(server_dict, file_ids, subfolder, verbose)



def download_from_ids(server_dict, file_ids: list, subfolder, verbose=False):
    """download files specified by list of seek-ids to a specified subfolder"""
    session = hf.create_session(server_dict['auth_dict'])
    successfully_downloaded_files = []
    unsuccessfully_downloaded_files = []
    for id_entry in file_ids:
        success, file_contents, file_name = hf.return_file_from_id(session, server_dict, id_entry, verbose)
        if success:
            save_to_disc(file_contents, file_name, subfolder)
            successfully_downloaded_files.append(file_name)
        else:
            unsuccessfully_downloaded_files.append(file_name)

    if unsuccessfully_downloaded_files:
        if verbose:
            print(f"The following {len(unsuccessfully_downloaded_files)} file(s) could not be downloaded:")
            for item in unsuccessfully_downloaded_files:
                print(f"\tFile: '{item}'")
        else:
            print(f"The above {len(unsuccessfully_downloaded_files)} file(s) could not be downloaded.")

    print(
        f"The following {len(successfully_downloaded_files)} files were successfully downloaded to folder '{subfolder}':")
    for item in successfully_downloaded_files:
        print(f"\tFile: {item}")




def save_to_disc_deprecated(file_contents, file_name, subfolder='downloads'):
    os.makedirs(subfolder, exist_ok=True)
    save_path = os.path.join(subfolder, file_name)
    save_path = hf.uniquify_path(save_path)
    encoding = "ANSI"#'cp1252'#'latin1'#'utf-8'#
    decoded = file_contents.decode(encoding)

    with open(save_path, 'wb', ) as f:#encoding=encoding    #wb instead of w correctly downloads excel file
        print(f)
        f.write(file_contents)
        #f.write(decoded)#


def save_to_disc(file_contents, save_path, subfolder=None):
    if subfolder:
        os.makedirs(subfolder, exist_ok=True)
        save_path = os.path.join(subfolder, save_path)

    # Stellen Sie sicher, dass der Zielordner existiert
    os.makedirs(os.path.dirname(save_path), exist_ok=True)

    save_path = hf.uniquify_path(save_path)
    encoding = "ANSI"  # 'cp1252'#'latin1'#'utf-8'#

    with open(save_path, 'wb') as f:
        print(f"Saving to: {save_path}")
        f.write(file_contents)

    return save_path  # Rückgabe des tatsächlichen Speicherpfads



def get_children_of_asset(server_dict, asset_id, type_of_asset, folder, exceptions):
    """
    This function recursively retrieves and processes the hierarchical structure of assets from a server, potentially downloading specific files at the lowest level of the hierarchy.
    :param server_dict: A dictionary containing server connection details and authentication information.
    :param asset_id: The unique identifier of the current asset being processed.
    :param type_of_asset: The type of the current asset (e.g., 'programmes', 'projects', 'investigations', etc.).
    :param folder: The local folder path where downloaded files or created subfolders will be stored.
    :param exceptions: A list of asset titles to be excluded from processing.
    """
    next_hierarchy = {'programmes': 'projects', 'projects': 'investigations', 'investigations': 'studies', 'studies': 'assays',
                      'assays': 'data_files'}
    title = hf.get_title_of_object(server_dict, asset_id, type_of_asset, verbose=False)
    print(f'we potentially want to download data files from {hf.singular_of(type_of_asset)}: {title}')
    link_to_asset = f'{server_dict["base_url"]}/{type_of_asset}/{asset_id}'
    session = hf.create_session(server_dict['auth_dict'])
    asset_response = session.get(link_to_asset, verify=server_dict['working_certificate'])
    relations = asset_response.json()['data']['relationships']

    for asset in relations[next_hierarchy[type_of_asset]]['data']:
        asset_id = asset['id']
        type_of_asset = asset['type']
        title = hf.get_title_of_object(server_dict, asset_id, type_of_asset, verbose=False)
        if title not in exceptions:
            if type_of_asset != 'data_files':
                subfolder = os.path.join(folder, title)
                # create folders with name of children assets
                os.makedirs(subfolder, exist_ok=True)
                get_children_of_asset(server_dict, asset_id, type_of_asset, subfolder, exceptions)
            else:
                #download the file
                if 'variants' in title and 'all' not in title and 'PDAC' not in title:
                    success, file_contents, file_name = hf.return_file_from_id(session, server_dict, asset_id, verbose=False)
                    if success:
                        save_to_disc(file_contents, file_name, folder)
                        #successfully_downloaded_files.append(file_name)
                    else:
                        print(f'could not download {file_name}')
