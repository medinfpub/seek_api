

import requests
import json
import string
import os
from seek_api_code import helper_functions as hf
from seek_api_code import download as dl
from seek_api_code import searching as s
from seek_api_code import servers

verbose = True


# the purpose of this script is to download all panel sequencing files




server_dict = servers.api_admin_server_dict#our_testing_server_dict#


# to find them we use 'kimia' as keyword because she uploaded all of the recent file
searchterm = 'Kimia'#'CDX_all_variants'#
#hf.return_file_from_id(server_dict, file_id, verbose)
subfolder = 'downloaded_files'


#s.return_ids_and_titles_from_seek_search(searchterm, server_dict, verbose)
#dl.bulk_download(searchterm, server_dict, subfolder, return_unique=True, searchmethod='seek_search', verbose=verbose)
#file_ids = ['6?version=10']#['6']#['1826']##['1869']
#https://seek.kfo5002.umg.eu/data_files/6?version=11

results_dict_to_keep, file_ids_to_keep = s.return_ids_and_titles_from_seek_search(searchterm, True, server_dict, verbose)
results_dict_all, file_ids_all = s.return_ids_and_titles_from_seek_search(searchterm, False, server_dict, verbose)

diff = results_dict_all.keys() - results_dict_to_keep
to_remove = dict()
for id in diff:
    to_remove[id] = results_dict_all[id]

#select only mutation files
mutation_files = {}
for k, v in results_dict_to_keep.items():
    if 'variants' in v and 'all' not in v and 'PDAC' not in v:
        mutation_files[k] = v

new_mutation_file_ids = list(mutation_files.keys())
dl.download_from_ids(server_dict, new_mutation_file_ids, subfolder, verbose=True)
print('dummy')




