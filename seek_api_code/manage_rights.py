from seek_api_code import helper_functions as hf


def give_all_manage_rights_to_account(server_dict, account_id, raise_error=True):
    base_url = server_dict['base_url']
    session = hf.create_session(server_dict['auth_dict'])
    entity_type = 'people'
    asset_type_list = ['data_files', 'assays', 'studies', 'investigations', 'projects', 'programmes']
    for asset_type in asset_type_list:
        list_of_all_assets = session.get(f'{base_url}/{asset_type}', verify=server_dict['working_certificate']).json()[
            'data']
        for asset in list_of_all_assets:
            asset_id = asset['id']
            link_to_file = base_url + f'/{asset_type}/{asset_id}'
            give_manage_rights_to_entity(server_dict, account_id, link_to_file, entity_type, raise_error)


def give_manage_rights_to_entity(server_dict, entity_id, link_to_file, entity_type='people', raise_error=False):
    session = hf.create_session(server_dict['auth_dict'])
    r = session.get(link_to_file)
    response_json = r.json()

    print(f'adding manage permission for account {entity_id} to asset {link_to_file}')
    # print(f"{response_json['data']['attributes']}")
    if 'policy' not in response_json['data']['attributes']:
        response_json['data']['attributes']['policy'] = {'permissions': []}
    response_json['data']['attributes']['policy']['permissions'].append(
        {'resource': {'id': entity_id, 'type': entity_type}, 'access': 'manage'})
    if entity_type == 'people':
        response_json['data']['relationships']['creators']['data'].append(
            {'id': entity_id, 'type': 'people'})
    else:
        if entity_type in response_json['data']['relationships']:
            response_json['data']['relationships'][entity_type]['data'].append({'id': entity_id, 'type': entity_type})
        else:
            response_json['data']['relationships'][entity_type] = {'data': [{'id': entity_id, 'type': entity_type}]}
    print(response_json['data']['relationships'][entity_type])
    response = session.patch(link_to_file, json=response_json,
                             verify=server_dict['working_certificate'])
    if response.status_code == 200:
        print('update successful')
    else:
        if raise_error:
            print(response.raise_for_status())
        else:
            print(f'silently passed error: {response.status_code}  for file {link_to_file}')


#    r = session.get(link_to_file)
#    response_json = r.json()
#    print(response_json['data']['attributes']['policy']['permissions'])
